# Docker in Docker CI image

This is a docker image meant to be used in a CI pipeline. At [NWON](https://nwon.de) we use it with the gitlab runner and the docker executor.

It basically adds some functionality that makes it more convenient to use (e.g. adding bash).

## Tag policy

We always use the tag of the [official docker image](https://hub.docker.com/_/docker/) that we used as a base image. On top we publish a `latest` version.

## Repository

The repository is hosted at Gitlab: [https://gitlab.com/nwon-group/nwon-docker-images/dind-ci.git](https://gitlab.com/nwon-group/nwon-docker-images/dind-ci.git).

## How to publish a new version

```bash
# Building and tagging the image
docker build . -t nwon/dind-ci:25.0.5-git

# Also add latest tag
docker tag nwon/dind-ci:25.0.5-git nwon/dind-ci:latest

# Push both image versions
docker push nwon/dind-ci:25.0.5-git
docker push nwon/dind-ci:latest
```
