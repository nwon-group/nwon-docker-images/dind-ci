FROM docker:25.0.5-git

RUN apk add --no-cache --upgrade perl wget bash

RUN sed -i -e "s/bin\/ash/bin\/bash/" /etc/passwd

CMD ["/bin/bash"]
